package ua.com.skywell.kotlinweather.core.network

/**
 * <p>Contains error message that will be shown to user.</p>
 *
 * @author chkv
 * @version 1
 * @since 02.06.2017
 */
data class ErrorMessage(val errorMessage: String)