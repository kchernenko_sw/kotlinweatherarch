package ua.com.skywell.kotlinweather.core.network.models

import com.google.gson.annotations.SerializedName

data class TodayWeatherModel(@SerializedName("dt") val dt: Int = 0,
                             @SerializedName("coord") val coordinates: Coordinates,
                             @SerializedName("visibility") val visibility: Int = 0,
                             @SerializedName("weather") val weather: List<WeatherItem>,
                             @SerializedName("name") val name: String = "",
                             @SerializedName("cod") val cod: Int = 0,
                             @SerializedName("main") val mainInfo: MainInfo,
                             @SerializedName("clouds") val clouds: Clouds,
                             @SerializedName("id") val id: Long = System.nanoTime(),
                             @SerializedName("sys") val metaInfo: MetaInfo,
                             @SerializedName("base") val base: String = "",
                             @SerializedName("wind") val wind: Wind
)