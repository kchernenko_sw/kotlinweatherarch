package ua.com.skywell.kotlinweather.core.storage.entities

import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteColumn
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteCreator
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteType
import ua.com.skywell.kotlinweather.core.storage.tables.WeatherTable

/**
 * <p>Mapped values from the [WeatherTable].</p>
 *
 * @author chkv
 * @version 1
 * @since 31.05.2017
 */
@StorIOSQLiteType(table = WeatherTable.TABLE_NAME)
data class WeatherEntity @StorIOSQLiteCreator constructor(
        @get:StorIOSQLiteColumn(name = WeatherTable.COLUMN_ID, key = true) val id: Long = System.nanoTime(),
        @get:StorIOSQLiteColumn(name = WeatherTable.TEMPERATURE) val temperature: Double,
        @get:StorIOSQLiteColumn(name = WeatherTable.MIN_TEMPERATURE) val minTemperature: Double,
        @get:StorIOSQLiteColumn(name = WeatherTable.MAX_TEMPERATURE) val maxTemperature: Double,
        @get:StorIOSQLiteColumn(name = WeatherTable.HUMIDITY) val humidity: Int,
        @get:StorIOSQLiteColumn(name = WeatherTable.PRESSURE) val pressure: Int,
        @get:StorIOSQLiteColumn(name = WeatherTable.CITY_ID) val cityId: Long
)