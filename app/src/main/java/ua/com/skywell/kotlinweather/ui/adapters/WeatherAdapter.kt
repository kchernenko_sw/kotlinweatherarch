package ua.com.skywell.kotlinweather.ui.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.weather_item.view.*
import ua.com.skywell.kotlinweather.R
import ua.com.skywell.kotlinweather.core.storage.entities.WeatherWithCity
import ua.com.skywell.kotlinweather.utils.ctx

/**
 * <p>Adapter for weather and related city list.</p>
 *
 * @author chkv
 * @version 1
 * @since 02.06.2017
 */
class WeatherAdapter: RecyclerView.Adapter<WeatherAdapter.ViewHolder>() {

    private var weatherList: List<WeatherWithCity> = ArrayList()
    private lateinit var itemClick: WeatherItemClickListener

    fun setWeather(weatherList: List<WeatherWithCity>) {
        this.weatherList = weatherList
        this.notifyDataSetChanged()
    }

    fun setItemClickListener(itemClick: WeatherItemClickListener) {
        this.itemClick = itemClick
    }

    fun getWeatherIdByPosition(position: Int): WeatherWithCity {
        return weatherList[position]
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.ctx).inflate(R.layout.weather_item, parent, false)
        return ViewHolder(view, itemClick)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindWeather(weatherList[position])
    }

    override fun getItemCount() = weatherList.size

    class ViewHolder(view: View, var itemClick: WeatherItemClickListener?) : RecyclerView.ViewHolder(view) {
        fun bindWeather(bindingResults: WeatherWithCity) {
            with(bindingResults) {
                itemView.temperature.text = bindingResults.weather.temperature.toString()
                itemView.minTemperature.text = bindingResults.weather.minTemperature.toString()
                itemView.maxTemperature.text = bindingResults.weather.maxTemperature.toString()
                itemView.humidity.text = bindingResults.weather.humidity.toString()
                itemView.pressure.text = bindingResults.weather.pressure.toString()
                itemView.cityName.text = bindingResults.city.name
                itemView.deleteBtn.setOnClickListener { itemClick?.onItemClicked(adapterPosition) }
            }
        }
    }
}