package ua.com.skywell.kotlinweather.core.storage.tables

/**
 * <p>Table for cities data.</p>
 *
 * @author const
 * @version 1
 * @since 05.06.2017
 */
class CityTable private constructor() {

    companion object {
        const val TABLE_NAME: String = "cities_table"
        const val COLUMN_ID: String = "city_id"
        const val CITY_NAME: String = "city_name"

        const val COLUMN_ID_WITH_TABLE_PREFIX = TABLE_NAME + "." + COLUMN_ID

        @JvmStatic
        fun getCreateTableQuery(): String {
            return "CREATE TABLE $TABLE_NAME ($COLUMN_ID INTEGER NOT NULL PRIMARY KEY, $CITY_NAME TEXT NOT NULL UNIQUE ON CONFLICT REPLACE);"
        }
    }
}