package ua.com.skywell.kotlinweather.ui.base

/**
 * <p>Base View interface-marker.</p>
 *
 * @author chkv
 * @version 1
 * @since 15.05.2017
 */
interface BaseView