package ua.com.skywell.kotlinweather.core.storage

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import ua.com.skywell.kotlinweather.core.storage.tables.CityTable
import ua.com.skywell.kotlinweather.core.storage.tables.WeatherTable
import ua.com.skywell.kotlinweather.injection.ApplicationContext
import javax.inject.Inject
import javax.inject.Singleton

/**
 * <p>Entry point for creating the database tables.</p>
 *
 * @author chkv
 * @version 1
 * @since 31.05.2017
 */
@Singleton
class DbOpenHelper @Inject constructor(@ApplicationContext context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    companion object {
        const val DATABASE_NAME: String = "weather.db"
        const val DATABASE_VERSION: Int = 1
        const val DROP_TABLE_QUERY = "DROP TABLE_NAME IF EXISTS "
    }

    override fun onCreate(db: SQLiteDatabase) {
        db.beginTransaction()
        try {
            db.execSQL(CityTable.getCreateTableQuery())
            db.execSQL(WeatherTable.getCreateTableQuery())
            db.setTransactionSuccessful()
        } finally {
            db.endTransaction()
        }
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.execSQL(DROP_TABLE_QUERY + CityTable.TABLE_NAME)
        db.execSQL(DROP_TABLE_QUERY + WeatherTable.TABLE_NAME)
        onCreate(db)
    }
}