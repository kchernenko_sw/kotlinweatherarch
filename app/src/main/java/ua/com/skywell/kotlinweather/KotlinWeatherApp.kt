package ua.com.skywell.kotlinweather

import android.app.Application
import android.support.annotation.VisibleForTesting
import com.facebook.stetho.Stetho
import com.squareup.leakcanary.LeakCanary
import timber.log.Timber
import ua.com.skywell.kotlinweather.injection.components.ApplicationComponent
import ua.com.skywell.kotlinweather.injection.components.DaggerApplicationComponent
import ua.com.skywell.kotlinweather.injection.modules.ApplicationModule

/**
 * <p>For libs initialization.</p>
 *
 * @author chkv
 * @version 1
 * @since 15.05.2017
 */
class KotlinWeatherApp: Application() {

    companion object{
        lateinit var applicationComponent: ApplicationComponent
    }

    override fun onCreate() {
        super.onCreate()
        initDagger()
        initTimber()
        initStetho()
        initLeakCanary()
    }

    @VisibleForTesting
    fun initDagger() {
        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(ApplicationModule(this))
                .build()
    }

    fun initTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

    fun initStetho() {
        if (BuildConfig.DEBUG) {
            Stetho.initializeWithDefaults(this)
        }
    }

    fun initLeakCanary() {
        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return
        }
        LeakCanary.install(this)
    }
}