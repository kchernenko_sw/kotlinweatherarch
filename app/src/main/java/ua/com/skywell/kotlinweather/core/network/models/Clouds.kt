package ua.com.skywell.kotlinweather.core.network.models

import com.google.gson.annotations.SerializedName

data class Clouds(@SerializedName("all") val all: Int = 0)