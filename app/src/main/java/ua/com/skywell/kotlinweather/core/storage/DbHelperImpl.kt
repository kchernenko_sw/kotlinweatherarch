package ua.com.skywell.kotlinweather.core.storage

import com.pushtorefresh.storio.sqlite.SQLiteTypeMapping
import com.pushtorefresh.storio.sqlite.StorIOSQLite
import com.pushtorefresh.storio.sqlite.impl.DefaultStorIOSQLite
import com.pushtorefresh.storio.sqlite.queries.DeleteQuery
import com.pushtorefresh.storio.sqlite.queries.RawQuery
import rx.Observable
import ua.com.skywell.kotlinweather.core.storage.entities.*
import ua.com.skywell.kotlinweather.core.storage.resolvers.WeatherWithCityDeleteResolver
import ua.com.skywell.kotlinweather.core.storage.resolvers.WeatherWithCityGetResolver
import ua.com.skywell.kotlinweather.core.storage.resolvers.WeatherWithCityPutResolver
import ua.com.skywell.kotlinweather.core.storage.tables.CityTable
import ua.com.skywell.kotlinweather.core.storage.tables.WeatherTable
import javax.inject.Inject
import javax.inject.Singleton

/**
 * <p>An implementation of the [DbHelper].</p>
 *
 * @author chkv
 * @version 1
 * @since 26.05.2017
 */
@Singleton
open class DbHelperImpl : DbHelper {

    private val storIOSQLite: StorIOSQLite

    @Suppress("ConvertSecondaryConstructorToPrimary")
    @Inject constructor(dbOpenHelper: DbOpenHelper) {
        storIOSQLite = DefaultStorIOSQLite.builder()
                .sqliteOpenHelper(dbOpenHelper)
                .addTypeMapping(CityEntity::class.java, CityEntitySQLiteTypeMapping())
                .addTypeMapping(WeatherEntity::class.java, WeatherEntitySQLiteTypeMapping())
                .addTypeMapping(WeatherWithCity::class.java, SQLiteTypeMapping.builder<WeatherWithCity>()
                        .putResolver(WeatherWithCityPutResolver())
                        .getResolver(WeatherWithCityGetResolver())
                        .deleteResolver(WeatherWithCityDeleteResolver())
                        .build())
                .build()
    }

    override fun saveTodayTemperature(weatherWithCity: WeatherWithCity): Int {
        return storIOSQLite
                .put()
                .objects(listOf(weatherWithCity))
                .withPutResolver(WeatherWithCityPutResolver())
                .prepare()
                .executeAsBlocking()
                .numberOfInserts()
    }

    override fun getTodayTemperature(): Observable<List<WeatherWithCity>> {
        return storIOSQLite
                .get()
                .listOfObjects(WeatherWithCity::class.java)
                .withQuery(RawQuery.builder()
                        .query("SELECT * FROM ${WeatherTable.TABLE_NAME} JOIN ${CityTable.TABLE_NAME}" +
                                " ON ${WeatherTable.COLUMN_CITY_ID_WITH_TABLE_PREFIX}" +
                                " = ${CityTable.COLUMN_ID_WITH_TABLE_PREFIX}")
                        .observesTables(WeatherTable.TABLE_NAME)
                        .build())
                .withGetResolver(WeatherWithCityGetResolver())
                .prepare()
                .asRxObservable()
    }

    override fun deleteWeather(weatherWithCity: WeatherWithCity): Observable<Boolean> {
        return Observable.just(storIOSQLite
                .delete()
                .objects(listOf(weatherWithCity))
                .withDeleteResolver(WeatherWithCityDeleteResolver())
                .useTransaction(true)
                .prepare()
                .executeAsBlocking()
                .wasDeleted(weatherWithCity))
    }
}