package ua.com.skywell.kotlinweather.core.network

/**
 *  <p>Data holder for server response or error.</p>
 *
 * @author chkv
 * @version 1
 * @since 02.06.2017
 */
data class ServerResponse<T> (var data: T? = null, var errorMessage: String? = null)