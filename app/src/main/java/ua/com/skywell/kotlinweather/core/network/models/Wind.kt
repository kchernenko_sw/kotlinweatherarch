package ua.com.skywell.kotlinweather.core.network.models

import com.google.gson.annotations.SerializedName

data class Wind(
        @SerializedName("deg") val deg: Double = 0.0,
        @SerializedName("speed") val speed: Double = 0.0,
        @SerializedName("gust") val gust: Double = 0.0
)