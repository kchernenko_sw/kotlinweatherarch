package ua.com.skywell.kotlinweather.core

import com.google.gson.Gson
import hu.akarnokd.rxjava.interop.RxJavaInterop
import io.reactivex.Flowable
import ua.com.skywell.kotlinweather.BuildConfig
import ua.com.skywell.kotlinweather.core.network.ErrorMessage
import ua.com.skywell.kotlinweather.core.network.ServerResponse
import ua.com.skywell.kotlinweather.core.network.WeatherApi
import ua.com.skywell.kotlinweather.core.storage.DbHelper
import ua.com.skywell.kotlinweather.core.storage.entities.CityEntity
import ua.com.skywell.kotlinweather.core.storage.entities.WeatherEntity
import ua.com.skywell.kotlinweather.core.storage.entities.WeatherWithCity
import javax.inject.Inject
import javax.inject.Singleton

/**
 * <p>An implementation of the @see MainRepository.</p>
 *
 * @author chkv
 * @version 1
 * @since 15.05.2017
 */
@Singleton
open class MainRepositoryImpl @Inject constructor(private val dbHelper: DbHelper,
                                                  private val weatherApi: WeatherApi,
                                                  private val gson: Gson): MainRepository {

    override fun loadTodayWeather(cityName: String): Flowable<ServerResponse<Boolean>> {
        val serverResponse = ServerResponse<Boolean>()
        return weatherApi.getTodayWeather(cityName, BuildConfig.API_KEY)
                .concatMap {
                    if (it.isSuccessful) {
                        val city = CityEntity(name = cityName)
                        dbHelper.saveTodayTemperature(WeatherWithCity(city = city,
                                weather = WeatherEntity(id = it.body()!!.id,
                                temperature = it.body()!!.mainInfo.temperature,
                                minTemperature = it.body()!!.mainInfo.minimumTemperature,
                                maxTemperature = it.body()!!.mainInfo.maximumTemperature,
                                humidity = it.body()!!.mainInfo.humidity,
                                pressure = it.body()!!.mainInfo.pressure,
                                cityId = city.id)))
                        serverResponse.data = true
                        Flowable.just(serverResponse)
                    } else {
                        serverResponse.errorMessage = gson.fromJson(it.errorBody()?.string(),
                                ErrorMessage::class.java).errorMessage
                        Flowable.just(serverResponse)
                    }
                }
    }

    override fun getTodayWeather(): Flowable<List<WeatherWithCity>> {
        return Flowable.defer { RxJavaInterop.toV2Flowable(dbHelper.getTodayTemperature()) }
    }

    override fun deleteWeather(weatherWithCity: WeatherWithCity): Flowable<Boolean> {
        return RxJavaInterop.toV2Flowable(dbHelper.deleteWeather(weatherWithCity))
    }
}