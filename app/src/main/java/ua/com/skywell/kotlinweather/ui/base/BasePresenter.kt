package ua.com.skywell.kotlinweather.ui.base

/**
 * <p>Base interface that provides helper methods for handling view state.</p>
 *
 * @author chkv
 * @version 1
 * @since 15.05.2017
 */
interface BasePresenter<V : BaseView> {

    fun attachView(view: V)

    fun detachView()

}