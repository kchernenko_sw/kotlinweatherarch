package ua.com.skywell.kotlinweather.ui.main

import android.util.Log
import ua.com.skywell.kotlinweather.core.storage.entities.WeatherEntity
import ua.com.skywell.kotlinweather.core.storage.entities.WeatherWithCity
import ua.com.skywell.kotlinweather.ui.base.BasePresenter
import ua.com.skywell.kotlinweather.ui.base.BaseView

/**
 * <p>View and presenter interfaces declaration.</p>
 *
 * @author chkv
 * @version 1
 * @since 15.05.2017
 */
interface MainContract {

    interface View: BaseView {
        fun showLoading(loader: Boolean)
        fun showWeather(weather: List<WeatherWithCity>)
        fun showError()
        fun showError(errorMsg : String)
    }

    interface Presenter: BasePresenter<View> {
        fun fetchWeatherData(city: String)
        fun getWeatherData()
        fun deleteWeather(weatherWithCity: WeatherWithCity)
    }
}