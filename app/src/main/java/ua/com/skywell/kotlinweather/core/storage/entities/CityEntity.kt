package ua.com.skywell.kotlinweather.core.storage.entities

import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteColumn
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteCreator
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteType
import ua.com.skywell.kotlinweather.core.storage.tables.CityTable

/**
 * <p>Represents mapped data from the [CityTable].</p>
 *
 * @author const
 * @version 1
 * @since 05.06.2017
 */
@StorIOSQLiteType(table = CityTable.TABLE_NAME)
data class CityEntity @StorIOSQLiteCreator constructor (
        @get:StorIOSQLiteColumn(name = CityTable.COLUMN_ID, key = true) val id: Long = System.nanoTime(),
        @get:StorIOSQLiteColumn(name = CityTable.CITY_NAME) val name: String)