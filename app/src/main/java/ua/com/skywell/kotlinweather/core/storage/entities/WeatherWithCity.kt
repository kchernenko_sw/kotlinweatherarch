package ua.com.skywell.kotlinweather.core.storage.entities

/**
 * <p>Holder object for both - weather and city.</p>
 *
 * @author const
 * @version 1
 * @since 05.06.2017
 */
data class WeatherWithCity(val city: CityEntity, val weather: WeatherEntity)