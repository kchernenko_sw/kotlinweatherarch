package ua.com.skywell.kotlinweather.core.storage.resolvers

import android.database.Cursor
import com.pushtorefresh.storio.sqlite.operations.get.DefaultGetResolver
import ua.com.skywell.kotlinweather.core.storage.entities.CityEntity
import ua.com.skywell.kotlinweather.core.storage.entities.WeatherEntity
import ua.com.skywell.kotlinweather.core.storage.entities.WeatherWithCity
import ua.com.skywell.kotlinweather.core.storage.tables.CityTable
import ua.com.skywell.kotlinweather.core.storage.tables.WeatherTable

/**
 * <p>Resolver for retrieving both - weather and city data.</p>
 *
 * @author const
 * @version 1
 * @since 05.06.2017
 */
class WeatherWithCityGetResolver: DefaultGetResolver<WeatherWithCity>() {

    override fun mapFromCursor(cursor: Cursor): WeatherWithCity {
        val city = CityEntity(id = cursor.getLong(cursor.getColumnIndexOrThrow(CityTable.COLUMN_ID)),
                name = cursor.getString(cursor.getColumnIndexOrThrow(CityTable.CITY_NAME)))

        val weather = WeatherEntity(id = cursor.getLong(cursor.getColumnIndexOrThrow(WeatherTable.COLUMN_ID)),
                temperature = cursor.getDouble(cursor.getColumnIndexOrThrow(WeatherTable.TEMPERATURE)),
                minTemperature = cursor.getDouble(cursor.getColumnIndexOrThrow(WeatherTable.MIN_TEMPERATURE)),
                maxTemperature = cursor.getDouble(cursor.getColumnIndexOrThrow(WeatherTable.MAX_TEMPERATURE)),
                humidity = cursor.getInt(cursor.getColumnIndexOrThrow(WeatherTable.HUMIDITY)),
                pressure = cursor.getInt(cursor.getColumnIndexOrThrow(WeatherTable.PRESSURE)),
                cityId = cursor.getLong(cursor.getColumnIndexOrThrow(WeatherTable.CITY_ID)))

        return WeatherWithCity(city, weather)
    }
}