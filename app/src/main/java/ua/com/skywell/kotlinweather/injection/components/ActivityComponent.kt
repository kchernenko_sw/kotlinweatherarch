package ua.com.skywell.kotlinweather.injection.components

import dagger.Subcomponent
import ua.com.skywell.kotlinweather.injection.ActivityScope
import ua.com.skywell.kotlinweather.injection.modules.ActivityModule
import ua.com.skywell.kotlinweather.ui.main.MainActivity

/**
 * <p>Component that is responsible for providing activity-related objects.</p>
 *
 * @author chkv
 * @version 1
 * @since 16.05.2017
 */
@ActivityScope
@Subcomponent(modules = arrayOf(ActivityModule::class))
interface ActivityComponent {

    fun inject(mainActivity: MainActivity)

}