package ua.com.skywell.kotlinweather.utils

import android.content.Context
import android.view.View
import android.widget.Toast

/**
 * <p>Extension functions related to the UI.</p>
 *
 * @author chkv
 * @version 1
 * @since 02.06.2017
 */
val View.ctx: Context
    get() = context

fun Context.simpleToast(msg: String, duration: Int = Toast.LENGTH_LONG) {
    Toast.makeText(this, msg, duration).show()
}