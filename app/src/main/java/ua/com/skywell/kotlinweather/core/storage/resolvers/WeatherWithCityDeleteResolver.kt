package ua.com.skywell.kotlinweather.core.storage.resolvers

import com.pushtorefresh.storio.sqlite.StorIOSQLite
import com.pushtorefresh.storio.sqlite.operations.delete.DeleteResolver
import com.pushtorefresh.storio.sqlite.operations.delete.DeleteResult
import ua.com.skywell.kotlinweather.core.storage.entities.WeatherWithCity
import ua.com.skywell.kotlinweather.core.storage.tables.CityTable
import ua.com.skywell.kotlinweather.core.storage.tables.WeatherTable
import java.util.Arrays.asList

/**
 * <p>Resolver for deleting both entries - weather entry and related city entry.</p>
 *
 * @author chkv
 * @version 1
 * @since 06.06.2017
 */
class WeatherWithCityDeleteResolver: DeleteResolver<WeatherWithCity>() {

    override fun performDelete(storIOSQLite: StorIOSQLite, obj: WeatherWithCity): DeleteResult {
        val deleteResult = storIOSQLite
                .delete()
                .objects(asList(obj.weather, obj.city))
                .prepare()
                .executeAsBlocking()

        val affectedTables: HashSet<String> = HashSet(2)

        affectedTables.add(WeatherTable.TABLE_NAME)
        affectedTables.add(CityTable.TABLE_NAME)

        return DeleteResult.newInstance(deleteResult.results().size, affectedTables)
    }
}