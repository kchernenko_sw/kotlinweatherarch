package ua.com.skywell.kotlinweather.ui.main

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import ua.com.skywell.kotlinweather.core.MainRepository
import ua.com.skywell.kotlinweather.core.storage.entities.WeatherWithCity
import javax.inject.Inject

/**
 * <p>Presenter for mainInfo screen.</p>
 *
 * @author chkv
 * @version 1
 * @since 16.05.2017
 */
@Suppress("ConvertSecondaryConstructorToPrimary")
open class MainPresenter : MainContract.Presenter {

    private var view: MainContract.View? = null
    private val compositeDisposable: CompositeDisposable
    private val mainRepository: MainRepository

    @Inject constructor(mainRepository: MainRepository) {
        this.mainRepository = mainRepository
        this.compositeDisposable = CompositeDisposable()
    }

    override fun attachView(view: MainContract.View) {
        this.view = view
    }

    override fun detachView() {
        view = null
        if (!compositeDisposable.isDisposed) compositeDisposable.clear()
    }

    override fun fetchWeatherData(city: String) {
        compositeDisposable.addAll(mainRepository.loadTodayWeather(city)
                .subscribeOn(Schedulers.io())
                .replay().autoConnect()
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { view?.showLoading(true) }
                .doAfterTerminate { view?.showLoading(false) }
                .subscribe({ result ->
                    val errorMsg = result.errorMessage
                    errorMsg?.let { view?.showError(it) }
                }, {
                    throwable ->
                    Timber.e(throwable, "Exception occurred while getting weather data!")
                    view?.showError()
                }))
    }

    override fun getWeatherData() {
        compositeDisposable.addAll(mainRepository.getTodayWeather()
                .replay().autoConnect()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    view?.showWeather(result)
                }, {
                    throwable ->
                    Timber.e(throwable, "Exception occurred while getting weather data!")
                    view?.showError()
                }))
    }

    override fun deleteWeather(weatherWithCity: WeatherWithCity) {
        compositeDisposable.addAll(mainRepository.deleteWeather(weatherWithCity)
                .subscribeOn(Schedulers.io())
                .replay().autoConnect()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->

                }, {
                    throwable ->
                    Timber.e(throwable, "Exception occurred while deleting weather data!")
                    view?.showError()
                }))
    }

}