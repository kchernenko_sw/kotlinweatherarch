package ua.com.skywell.kotlinweather.ui.adapters

/**
 * <p>Click listener for the [WeatherAdapter].</p>
 *
 * @author chkv
 * @version 1
 * @since 07.06.2017
 */
interface WeatherItemClickListener {

    fun onItemClicked(position: Int)

}