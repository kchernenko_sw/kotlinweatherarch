package ua.com.skywell.kotlinweather.injection

import javax.inject.Qualifier

/**
 * <p>Qualifier for marking Activity's context.</p>
 *
 * @author chkv
 * @version 1
 * @since 16.05.2017
 */
@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class ActivityContext