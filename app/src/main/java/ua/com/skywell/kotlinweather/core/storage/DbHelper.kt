package ua.com.skywell.kotlinweather.core.storage

import rx.Observable
import ua.com.skywell.kotlinweather.core.storage.entities.WeatherWithCity

/**
 * <p>Database-related layer.</p>
 *
 * @author chkv
 * @version 1
 * @since 26.05.2017
 */
interface DbHelper {

    fun saveTodayTemperature(weatherWithCity: WeatherWithCity): Int

    fun getTodayTemperature(): Observable<List<WeatherWithCity>>

    fun deleteWeather(weatherWithCity: WeatherWithCity): Observable<Boolean>
}