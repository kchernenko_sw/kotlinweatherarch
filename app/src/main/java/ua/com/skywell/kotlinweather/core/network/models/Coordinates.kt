package ua.com.skywell.kotlinweather.core.network.models

import com.google.gson.annotations.SerializedName

data class Coordinates(@SerializedName("lon") val longitude: Double = 0.0,
                       @SerializedName("lat") val latitude: Double = 0.0)