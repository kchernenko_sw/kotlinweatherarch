package ua.com.skywell.kotlinweather.core

import io.reactivex.Flowable
import ua.com.skywell.kotlinweather.core.network.ServerResponse
import ua.com.skywell.kotlinweather.core.storage.entities.WeatherEntity
import ua.com.skywell.kotlinweather.core.storage.entities.WeatherWithCity

/**
 * <p>Entry point for all business logic.</p>
 *
 * @author chkv
 * @version 1
 * @since 15.05.2017
 */
interface MainRepository {

    fun loadTodayWeather(cityName: String): Flowable<ServerResponse<Boolean>>

    fun getTodayWeather(): Flowable<List<WeatherWithCity>>

    fun deleteWeather(weatherWithCity: WeatherWithCity): Flowable<Boolean>
}