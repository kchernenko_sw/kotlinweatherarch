package ua.com.skywell.kotlinweather.injection

import javax.inject.Scope

/**
 * <p>Scope for Activity-related objects.</p>
 *
 * @author chkv
 * @version 1
 * @since 16.05.2017
 */
@MustBeDocumented
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ActivityScope