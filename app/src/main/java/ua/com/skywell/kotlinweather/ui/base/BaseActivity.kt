package ua.com.skywell.kotlinweather.ui.base

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import ua.com.skywell.kotlinweather.KotlinWeatherApp
import ua.com.skywell.kotlinweather.injection.components.ActivityComponent
import ua.com.skywell.kotlinweather.injection.modules.ActivityModule

/**
 * <p>Base activity for DI.</p>
 *
 * @author chkv
 * @version 1
 * @since 15.05.2017
 */
open class BaseActivity: AppCompatActivity() {

    lateinit var activityComponent: ActivityComponent
        get

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    protected fun addActivityComponent(): ActivityComponent {
        activityComponent = KotlinWeatherApp
                .applicationComponent
                .addActivityComponent(ActivityModule(this))

        return activityComponent
    }

}