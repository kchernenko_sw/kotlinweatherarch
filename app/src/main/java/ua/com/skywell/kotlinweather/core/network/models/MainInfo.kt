package ua.com.skywell.kotlinweather.core.network.models

import com.google.gson.annotations.SerializedName

data class MainInfo(
        @SerializedName("temp") val temperature: Double = Double.NaN,
        @SerializedName("temp_min") val minimumTemperature: Double = Double.NaN,
        @SerializedName("humidity") val humidity: Int = 0,
        @SerializedName("pressure") val pressure: Int = 0,
        @SerializedName("temp_max") val maximumTemperature: Double = Double.NaN
)