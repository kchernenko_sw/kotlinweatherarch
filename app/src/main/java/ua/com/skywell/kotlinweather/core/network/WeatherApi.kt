package ua.com.skywell.kotlinweather.core.network

import io.reactivex.Flowable
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query
import ua.com.skywell.kotlinweather.core.network.models.TodayWeatherModel

/**
 * <p>All request.</p>
 *
 * @author chkv
 * @version 1
 * @since 30.05.2017
 */
interface WeatherApi {

    @GET("data/2.5/weather")
    fun getTodayWeather(@Query("q") city: String,
                        @Query("appid") apiKey: String): Flowable<Response<TodayWeatherModel>>
}