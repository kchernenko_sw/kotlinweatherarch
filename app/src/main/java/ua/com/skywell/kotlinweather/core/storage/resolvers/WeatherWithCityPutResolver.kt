package ua.com.skywell.kotlinweather.core.storage.resolvers

import com.pushtorefresh.storio.sqlite.StorIOSQLite
import com.pushtorefresh.storio.sqlite.operations.put.PutResolver
import com.pushtorefresh.storio.sqlite.operations.put.PutResult
import com.pushtorefresh.storio.sqlite.operations.put.PutResults
import ua.com.skywell.kotlinweather.core.storage.entities.WeatherWithCity
import ua.com.skywell.kotlinweather.core.storage.tables.CityTable
import ua.com.skywell.kotlinweather.core.storage.tables.WeatherTable

/**
 * <p>Resolver for saving both objects - weather and city.</p>
 *
 * @author const
 * @version 1
 * @since 05.06.2017
 */
class WeatherWithCityPutResolver: PutResolver<WeatherWithCity>() {

    override fun performPut(storIOSQLite: StorIOSQLite, obj: WeatherWithCity): PutResult {
        val putResults: PutResults<Any> = storIOSQLite
                .put()
                .objects(listOf(obj.city, obj.weather))
                .prepare()
                .executeAsBlocking()

        val affectedTables: HashSet<String> = HashSet(2)

        affectedTables.add(CityTable.TABLE_NAME)
        affectedTables.add(WeatherTable.TABLE_NAME)

        return PutResult.newInsertResult(putResults.numberOfInserts().toLong(), affectedTables)
    }

}