package ua.com.skywell.kotlinweather.core.storage.tables


/**
 * <p>Table creation and querying constants.</p>
 *
 * @author chkv
 * @version 1
 * @since 31.05.2017
 */
class WeatherTable private constructor() {

    companion object {
        const val TABLE_NAME: String = "weather_table"
        const val COLUMN_ID: String = "weather_id"
        const val TEMPERATURE: String = "temperature"
        const val MIN_TEMPERATURE: String = "min_temperature"
        const val MAX_TEMPERATURE: String = "max_temperature"
        const val HUMIDITY: String = "humidity"
        const val PRESSURE: String = "pressure"
        const val CITY_ID: String = "city_id"

        const val COLUMN_CITY_ID_WITH_TABLE_PREFIX = TABLE_NAME + "." + CITY_ID

        @JvmStatic
        fun getCreateTableQuery(): String {
            return "CREATE TABLE $TABLE_NAME ($COLUMN_ID INTEGER NOT NULL PRIMARY KEY, $TEMPERATURE REAL NOT NULL, " +
                    "$MIN_TEMPERATURE REAL NOT NULL, $MAX_TEMPERATURE REAL NOT NULL, $HUMIDITY INTEGER NOT NULL, " +
                    "$PRESSURE INTEGER NOT NULL, $CITY_ID INTEGER NOT NULL, FOREIGN KEY ($CITY_ID) " +
                    "REFERENCES ${CityTable.TABLE_NAME}(${CityTable.COLUMN_ID}))"
        }
    }
}